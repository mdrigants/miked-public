USE [master]
GO
/****** Object:  Database [Millcreek]    Script Date: 4/2/2021 12:26:05 AM ******/
CREATE DATABASE [Millcreek]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Millcreek', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Millcreek.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Millcreek_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Millcreek_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Millcreek] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Millcreek].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Millcreek] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Millcreek] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Millcreek] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Millcreek] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Millcreek] SET ARITHABORT OFF 
GO
ALTER DATABASE [Millcreek] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Millcreek] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Millcreek] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Millcreek] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Millcreek] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Millcreek] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Millcreek] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Millcreek] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Millcreek] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Millcreek] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Millcreek] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Millcreek] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Millcreek] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Millcreek] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Millcreek] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Millcreek] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Millcreek] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Millcreek] SET RECOVERY FULL 
GO
ALTER DATABASE [Millcreek] SET  MULTI_USER 
GO
ALTER DATABASE [Millcreek] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Millcreek] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Millcreek] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Millcreek] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Millcreek] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Millcreek] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Millcreek', N'ON'
GO
ALTER DATABASE [Millcreek] SET QUERY_STORE = OFF
GO
USE [Millcreek]
GO
/****** Object:  Table [dbo].[Campsites]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Campsites](
	[CampId] [int] IDENTITY(1,1) NOT NULL,
	[Campsite] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Campsites] PRIMARY KEY CLUSTERED 
(
	[CampId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reservations]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reservations](
	[ReservationId] [int] IDENTITY(1,1) NOT NULL,
	[ReservationTime] [date] NOT NULL,
	[ReservationAvailable] [bit] NOT NULL,
	[CampId] [int] NOT NULL,
 CONSTRAINT [PK_Reservations] PRIMARY KEY CLUSTERED 
(
	[ReservationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AvailableCampsiteDates]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AvailableCampsiteDates]
AS
SELECT dbo.Reservations.ReservationTime, dbo.Campsites.Campsite
FROM     dbo.Reservations INNER JOIN
                  dbo.Campsites ON dbo.Reservations.CampId = dbo.Campsites.CampId
WHERE  (dbo.Reservations.ReservationAvailable = 1)
GO
/****** Object:  Table [dbo].[Visitors]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visitors](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[NumberOfVisitors] [int] NOT NULL,
	[DayofWeek] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Visitors] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[MostPopularDay]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike
-- Create date: 
-- Description:	Function to show most popular day to visit
-- =============================================
CREATE FUNCTION [dbo].[MostPopularDay] 
(	
	-- Add the parameters for the function here
	  
	 
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
select top 1
DayofWeek, avg(NumberOfVisitors) as AverageVistors
from Visitors
where [NumberOfVisitors] > 1
group by DayofWeek
order by AverageVistors desc
)
GO
/****** Object:  Table [dbo].[CamperReservations]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CamperReservations](
	[CamperId] [int] IDENTITY(1,1) NOT NULL,
	[CamperName] [nvarchar](256) NOT NULL,
	[ReservationId] [int] NOT NULL,
 CONSTRAINT [PK_Campers] PRIMARY KEY CLUSTERED 
(
	[CamperId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CamperReservations] ON 

INSERT [dbo].[CamperReservations] ([CamperId], [CamperName], [ReservationId]) VALUES (1, N'Authur Morgan', 1)
INSERT [dbo].[CamperReservations] ([CamperId], [CamperName], [ReservationId]) VALUES (2, N'John Marston', 3)
INSERT [dbo].[CamperReservations] ([CamperId], [CamperName], [ReservationId]) VALUES (3, N'Charles Smith', 4)
INSERT [dbo].[CamperReservations] ([CamperId], [CamperName], [ReservationId]) VALUES (4, N'Leviticus Cornwall', 5)
INSERT [dbo].[CamperReservations] ([CamperId], [CamperName], [ReservationId]) VALUES (5, N'Sadie Adler', 7)
SET IDENTITY_INSERT [dbo].[CamperReservations] OFF
GO
SET IDENTITY_INSERT [dbo].[Campsites] ON 

INSERT [dbo].[Campsites] ([CampId], [Campsite]) VALUES (1, N'Valentine')
INSERT [dbo].[Campsites] ([CampId], [Campsite]) VALUES (2, N'Rhodes')
INSERT [dbo].[Campsites] ([CampId], [Campsite]) VALUES (3, N'Strawberry')
INSERT [dbo].[Campsites] ([CampId], [Campsite]) VALUES (4, N'ThunderHawk')
INSERT [dbo].[Campsites] ([CampId], [Campsite]) VALUES (5, N'NewVegas')
SET IDENTITY_INSERT [dbo].[Campsites] OFF
GO
SET IDENTITY_INSERT [dbo].[Reservations] ON 

INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (1, CAST(N'2021-04-12' AS Date), 1, 3)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (2, CAST(N'2021-04-12' AS Date), 0, 2)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (3, CAST(N'2021-04-13' AS Date), 1, 1)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (4, CAST(N'2021-04-13' AS Date), 1, 4)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (5, CAST(N'2021-04-12' AS Date), 1, 1)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (6, CAST(N'2021-04-12' AS Date), 1, 5)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (7, CAST(N'2021-04-13' AS Date), 1, 5)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (9, CAST(N'2021-04-14' AS Date), 1, 2)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (10, CAST(N'2021-04-14' AS Date), 1, 3)
INSERT [dbo].[Reservations] ([ReservationId], [ReservationTime], [ReservationAvailable], [CampId]) VALUES (11, CAST(N'2021-04-15' AS Date), 1, 4)
SET IDENTITY_INSERT [dbo].[Reservations] OFF
GO
SET IDENTITY_INSERT [dbo].[Visitors] ON 

INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (1, CAST(N'2021-04-01' AS Date), 200, N'Thursday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (2, CAST(N'2021-04-02' AS Date), 300, N'Friday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (3, CAST(N'2021-04-03' AS Date), 350, N'Saturday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (4, CAST(N'2021-04-04' AS Date), 325, N'Sunday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (5, CAST(N'2021-04-05' AS Date), 100, N'Monday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (6, CAST(N'2021-04-06' AS Date), 75, N'Tuesday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (7, CAST(N'2021-04-07' AS Date), 80, N'Wednesday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (8, CAST(N'2021-04-08' AS Date), 150, N'Thursday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (9, CAST(N'2021-04-09' AS Date), 275, N'Friday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (10, CAST(N'2021-04-10' AS Date), 315, N'Saturday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (11, CAST(N'2021-04-11' AS Date), 68, N'Monday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (12, CAST(N'2021-04-12' AS Date), 62, N'Tuesday')
INSERT [dbo].[Visitors] ([RowId], [Date], [NumberOfVisitors], [DayofWeek]) VALUES (13, CAST(N'2021-04-13' AS Date), 71, N'Wednesday')
SET IDENTITY_INSERT [dbo].[Visitors] OFF
GO
ALTER TABLE [dbo].[Reservations] ADD  CONSTRAINT [DF_Reservations_ReservationAvailable]  DEFAULT ((1)) FOR [ReservationAvailable]
GO
ALTER TABLE [dbo].[CamperReservations]  WITH CHECK ADD  CONSTRAINT [FK_Campers_Reservations] FOREIGN KEY([ReservationId])
REFERENCES [dbo].[Reservations] ([ReservationId])
GO
ALTER TABLE [dbo].[CamperReservations] CHECK CONSTRAINT [FK_Campers_Reservations]
GO
ALTER TABLE [dbo].[Reservations]  WITH CHECK ADD  CONSTRAINT [FK_Reservations_Campsites] FOREIGN KEY([CampId])
REFERENCES [dbo].[Campsites] ([CampId])
GO
ALTER TABLE [dbo].[Reservations] CHECK CONSTRAINT [FK_Reservations_Campsites]
GO
/****** Object:  StoredProcedure [dbo].[DeleteCamperReservation]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike Drigants
-- Create date: 
-- Description:	Delete record from CamperReservations
-- =============================================
CREATE PROCEDURE [dbo].[DeleteCamperReservation] 
	-- Add the parameters for the stored procedure here
	@CamperName nvarchar(256) = 0, 
	@ReservationId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from dbo.CamperReservations
	where CamperName= @CamperName and ReservationId = @ReservationId
END
GO
/****** Object:  StoredProcedure [dbo].[InsertCamperReservation]    Script Date: 4/2/2021 12:26:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike
-- Create date: 
-- Description:	Insert new row into Campers Reservations table
-- =============================================
CREATE PROCEDURE [dbo].[InsertCamperReservation] 
	-- Add the parameters for the stored procedure here
	@CamperName nvarchar(256) = 0, 
	@ReservationId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into dbo.CamperReservations
	(CamperName,
	ReservationId
	)
	values
	(
		@CamperName,
		@ReservationId
	)

END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Reservations"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Campsites"
            Begin Extent = 
               Top = 7
               Left = 330
               Bottom = 126
               Right = 524
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2400
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AvailableCampsiteDates'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AvailableCampsiteDates'
GO
USE [master]
GO
ALTER DATABASE [Millcreek] SET  READ_WRITE 
GO
