﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQuestionsWillis
{
    class UniqueEmail
    {
        static void Main(string[] args)
        {
            string[] emails = { "first.m.last@somewhere.com", "firstmlast@somewhere.com", "team.1+bob@somewhere.com", "team1+jill+bob@somewhere.com",
            "team1@somewhere.com", "team2@somewhere.com", "team2@some.where.com"};
            Solution solution = new Solution();
            int uniqueCount = solution.NumberOfUniqueEmailAddresses(emails);
        }
    }
}
public class Solution
{
    /// <summary>
    /// Count out all the unique emails as per the instructions with the special case of tha characters '+' and '.'
    /// </summary>
    /// <param name="emails"></param>
    /// <returns name="uniqueCount"></returns>
    public int NumberOfUniqueEmailAddresses(string[] emails) 
    {
        List<string> convertedEmails = new List<string>();
        foreach(string anEmail in emails)
        {
            if (anEmail.Count(c=>c == '@') == 1) //multiple @ in an email is outside of scope so will ignore those that have it
            {
                string[] splitter = anEmail.Split('@'); //splitting because I can ignore right of @
                string firstPart = splitter[0].Replace(".", "");
                if (firstPart.Contains('+'))
                    firstPart = firstPart.Substring(0, firstPart.IndexOf('+'));
                string converted = firstPart + "@" + splitter[1];
                if (!convertedEmails.Contains(converted))
                    convertedEmails.Add(converted);
            }
        }
        return convertedEmails.Count();
    }
}
