﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {

        static void Main(string[] args)
        {
            int gridSize; //set to grid size desired
            Console.WriteLine("Select size of game by typing an integer and hitting enter");
            gridSize = int.Parse(Console.ReadLine()); //let be user selectable
            TicTacToe ticTacToe = new TicTacToe(gridSize);


        }
    }
    class TicTacToe
    {
        public int[,] GameGrid;
        public int StartSize;
       // private bool playerOneTurn = true;
        /// <summary>
        /// Created a Tic Tac Tow game board
        /// </summary>
        /// <param name="n">nxn dimension for the game board</param>
        public TicTacToe(int n)
        {
            //Array.Clear(GameGrid, 0, GameGrid.Length);
            bool winner = false;
            GameGrid = new int[n, n];
            StartSize = n;
            int player = 1;
            int turnsInGame = 0;
            do
            {
                Redraw(player.ToString());
                string playerChoice = "";
                bool validPlay = false;
                int rowSelect = -1;
                int colSelect = -1;
                while (validPlay == false)
                    validPlay = CheckPlayerInput(ref playerChoice, ref rowSelect, ref colSelect);
                
                //update row
                int gameState = PlacePiece(rowSelect, colSelect, player);
                turnsInGame++;
                if (gameState == 1 || gameState == 2)
                {
                    winner = true;
                    Redraw(player.ToString());
                    if (gameState == 1)
                        Console.WriteLine("Player 1 wins");
                    else
                        Console.WriteLine("Player 2 wins");
                }
                //cannot place any more pieces and game is over with no winner
                if(turnsInGame == StartSize * StartSize)
                {
                    Console.WriteLine("No more valid moves can be made and the game is a draw");
                    winner = true;
                }
                if (player == 1)
                    player = 2;
                else
                    player = 1;
            } while (!winner);
            string holdLine = Console.ReadLine();
        }
        /// <summary>
        /// Clears the board and set up to call DrawBoard
        /// </summary>
        /// <param name="player"></param>
        private void Redraw(string player)
        {
            Console.Clear();
            Console.WriteLine("Player1:1 and Player2:2");
            Console.WriteLine("\n");
            Console.WriteLine("It is Player{player} Turn");
            DrawBoard();
        }

        /// <summary>
        /// Checks to make sure player input is valid. This is probably not neccesary for the code review but is an example of checking user inputs
        /// </summary>
        /// <param name="playerChoice"></param>
        /// <param name="rowSelect"></param>
        /// <param name="colSelect"></param>
        /// <returns>"False and it keeps forcing user to reenter value. True is accepted and moves on</returns>
        private bool CheckPlayerInput(ref string playerChoice, ref int rowSelect, ref int colSelect)
        {
            playerChoice = Console.ReadLine();
            if (!playerChoice.Contains(","))
            {
                Console.WriteLine("Please make a valid choice of game area, format is N,N and shown on gamegrid");
                return false;
            }
            string[] splitter = playerChoice.Split(',');
            int rowParse;
            bool rowCanParse = int.TryParse(splitter[0], out rowParse);
            int colParse;
            bool colCanParse = int.TryParse(splitter[1], out colParse);
            if(!rowCanParse || !colCanParse)
            {
                Console.WriteLine("Please make a valid choice of game area, check numbers that were entered must all be integers in a N,N format");
                return false;
            }
            //check to see if already taken
            if(rowParse >= StartSize || colParse >= StartSize)
            {
                Console.WriteLine("Please make a choice that is inside the bounds of the game area");
                return false;
            }
            int choiceValue = GameGrid[rowParse, colParse];
            if(choiceValue != 0)
            {
                Console.WriteLine("The choice you have made has already been taken. Please make another choice");
                return false;
            }
            rowSelect = rowParse;
            colSelect = colParse;
            //choice is valid
            return true;
        }
        
        /// <summary>
        /// Draws the game board
        /// </summary>
        private void DrawBoard()
        {
            int rowCount = 0;
            int colCount = 0;
            string aLine = $"{rowCount},{colCount} {GameGrid[rowCount, colCount]}     ";
            while (colCount < StartSize && rowCount < StartSize)
            {
                if (colCount == StartSize - 1 && rowCount != StartSize - 1)
                {
                    colCount = 0;
                    rowCount++;
                    aLine = aLine + Environment.NewLine;
                }
                else if(colCount == StartSize - 1 && rowCount == StartSize - 1)
                {
                    Console.WriteLine(aLine);
                    return;
                }
                else
                    colCount++;
                aLine = aLine + $"{rowCount},{colCount} {GameGrid[rowCount, colCount]}     ";

            }
        }

        /// <summary>
        /// Place a piece on the game board
        /// </summary>
        /// <param name="row">row to place a piece</param>
        /// <param name="col">column to place a piece</param>
        /// <param name="player">the player (1 or 2) the piece is for</param>
        /// <returns>0 = no winner, 1 = player 1 won, 2 = player 2 won</returns>
        public int PlacePiece(int row, int col, int player)
        {
            int gameState = 0;
            GameGrid[row, col] = player;
            gameState = CheckWin(row, col, player);
            return gameState;
        }

        //only looking for wins on active players turn, also only looking at row and column and then looking to see if the \ and / are valid
        //vs checking every combination every time
        public int CheckWin(int row, int col, int player)
        {
            //check row win first
            int colCount = 0;
            List<bool> rowCheck = new List<bool>();
            while(colCount < StartSize)
            {
                if (GameGrid[row, colCount] == player)
                    rowCheck.Add(true);
                else
                    rowCheck.Add(false);
                colCount++;
            }
            if (!rowCheck.Contains(false))
                return player;

            //check col win condition
            int rowCount = 0;
            List<bool> colCheck = new List<bool>();
            while (rowCount < StartSize)
            {
                if (GameGrid[rowCount, col] == player)
                    colCheck.Add(true);
                else
                    colCheck.Add(false);
                rowCount++;
            }
            if (!colCheck.Contains(false))
                return player;

            //do the \ /
            colCount = 0;
            rowCount = 0;
            List<bool> angleCheck = new List<bool>();
            while (colCount < StartSize)
            {
                if (GameGrid[rowCount, colCount] == player)
                    angleCheck.Add(true);
                else
                    angleCheck.Add(false);
                colCount++;
                rowCount++;
            }
            if (!angleCheck.Contains(false))
                return player;
            colCount = 0;
            rowCount = StartSize -1;
            angleCheck.Clear();
            while (colCount < StartSize)
            {
                if (GameGrid[rowCount, colCount] == player)
                    angleCheck.Add(true);
                else
                    angleCheck.Add(false);
                colCount++;
                rowCount--;
            }
            if (!angleCheck.Contains(false))
                return player;

            //if still false will return 0
            return 0;
        }

    }
}
